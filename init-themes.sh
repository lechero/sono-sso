#!/bin/bash
THEMES_PATH="docker-data/themes"
rm -rf "$THEMES_PATH/bepop"
rm -rf "$THEMES_PATH/child-theme"
unzip bepop-theme.zip -d "$THEMES_PATH" && rm -rf "$THEMES_PATH/__MACOSX"
mv "$THEMES_PATH/bepop" "$THEMES_PATH/child-theme"
echo "MOVED"
unzip bepop-theme.zip -d "$THEMES_PATH" && rm -rf "$THEMES_PATH/__MACOSX"
echo "EXPANDED bepop"
unzip -o bepop.child-theme.zip -d "$THEMES_PATH" && rm -rf "$THEMES_PATH/__MACOSX"
cat readme.md 