<?php
/*
Plugin Name: WordPress SonoSuite SSO
Plugin URI:  https://github.com/simonrcodrington/Introduction-to-WordPress-Plugins---Location-Plugin
Description: Enable SonoSuite Single sign on in wordpress
Version:     1.0.5
Author:      Fuentastic
Author URI:  http://fuentastic.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

defined( 'ABSPATH' ) or die( 'Nope, not accessing this' );

if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    $loader = require __DIR__ . '/vendor/autoload.php';
    $loader->addPsr4('Firebase\\JWT\\', __DIR__);
}

function writeToFile($str) {
    try{
        $path = __DIR__ ."/sono-sso.log";
        $myfile = fopen($path, "a") or die("Unable to open file!");
        fwrite($myfile, $str."\n");
        fclose($myfile);
    }catch(\Exception $e){
        echo "brrr";
    } 
}

function bler($msg, $data = []) {
    $op = "$msg\n";
    if(!empty($data))  $op = "$msg:".json_encode($data, JSON_PRETTY_PRINT)."\n";
    //echo "bler: ".$op."<br/>\n";
    writeToFile($op);
}

function getJWT($user) {
    $payload = [
        'exp' => (string)(time()+100000),
        'iat' => (string)$now = time(),
        'jti' => md5(($now). mt_rand()),
        'email' => $user->user_email,
        'externalId' => (string)$user->ID
    ];
    
    $jwt = \Firebase\JWT\JWT::encode($payload, $secret);
    return $jwt;
}

function sonosuite_sso_login_redirect_callback( $redirect_to, $requested_redirect_to, $user ) {
    $sonosuite_url = get_option( 'sonosso_sonosuite_url', '' );
   
    $jwt = getJWT($user);
    $url = $sonosuite_url."?jwt=".$jwt;

    bler("----------- BEGIN sonosuite_sso_login_redirect_callback -----------");
    bler("redirect_to:$redirect_to");
    bler("requested_redirect_to:$requested_redirect_to");
    bler("url:$url");
    bler("----------- END sonosuite_sso_login_redirect_callback -----------");

    return $url;
}

function sonosuite_sso_login_check_callback($user_login, $user) {
    $secret = get_option( 'sonosso_secret', '' );
    $login_url = get_option( 'sonosso_login_url', '' );
    $sonosuite_url = get_option( 'sonosso_sonosuite_url', '' );
    $enabled = get_option( 'sonosso_enabled', '' );
    $actual_host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $actual_url = $actual_host.$_SERVER['PHP_SELF'];

    bler("----------- BEGIN sonosuite_sso_login_redirect_callback -----------");

    $data = [
        'login_url' => $login_url,
        'wp_get_referer' => wp_get_referer(),
        'sonosuite_url' => $sonosuite_url,
        'enabled' => $enabled,
        'actual_host' => $actual_host,
        'actual_url' => $actual_url,
        'user' => $user
    ];

    bler("sonosuite_sso_login_check_callback", $data);

    //defined( 'DOING_AJAX' ) && DOING_AJAX

    if($enabled === 'checked' && wp_get_referer() === $login_url) {
        
        if(!wp_doing_ajax()){
            $jwt = getJWT($user);
            $url = $sonosuite_url."?jwt=".$jwt;
            wp_redirect($url);
            bler("starting redirect -> Bbye! ".$url);
            bler("----------- END sonosuite_sso_login_redirect_callback -----------");
            exit();
        }
        
        add_filter( 'login_redirect', 'sonosuite_sso_login_redirect_callback', 1, 3 );
        add_filter( 'login_redirect', 'sonosuite_sso_login_redirect_callback', 9, 3 );
    }
}
add_action('wp_login', 'sonosuite_sso_login_check_callback', 1, 2 );

function sonosuite_sso_admin_init() {
    if(!empty($_POST)) {
        update_option( 'sonosso_secret', $_POST['sonosso_secret'] );
        update_option( 'sonosso_enabled', $_POST['sonosso_enabled'] );
        update_option( 'sonosso_login_url', $_POST['sonosso_login_url'] );
        update_option( 'sonosso_sonosuite_url', $_POST['sonosso_sonosuite_url'] );
        echo "<div>Saved!</div>";
    }

    $actual_host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
    $actual_link = $actual_host.$_SERVER[REQUEST_URI];
    
    $secret = get_option( 'sonosso_secret', '' );
    $login_url = get_option( 'sonosso_login_url', '' );
    $sonosuite_url = get_option( 'sonosso_sonosuite_url', '' );
    $enabled = get_option( 'sonosso_enabled', '' );
    ?>
    <form method="post" action="<?php echo $actual_link; ?>">
        <h1>Sono Suite SSO!</h1>
        <div>
            SSO enabled: <input name="sonosso_enabled" type="checkbox" value="checked" <?php echo $enabled;?>/>
        </div>
        <div>
            Secret: <input width="500px;" size="50" name="sonosso_secret" value="<?php echo $secret;?>"/>
        </div>
        <div>
            Sonosuite Login Url: <input width="500px;" size="50" placeholder="<?php echo $actual_host;?>/login" name="sonosso_login_url" value="<?php echo $login_url;?>"/>
        </div>
        <div>
            Sonosuite Url: <input width="500px;" size="50" placeholder="<?php echo $actual_host;?>" name="sonosso_sonosuite_url" value="<?php echo $sonosuite_url;?>"/>
        </div>
        <div>
            <input type="submit" value="Update">
        </div>
    </form>
    <br/>
    <br/>
    <a href="http://fuentastic.com" target="_blank">Fuentastic - 2020</a>
    <?php
}
 
function sonosuite_sso_setup_menu(){
    add_menu_page( 'SonoSuite SSO', 'SonoSuite SSO', 'manage_options', 'sono-suite-sso-plugin', 'sonosuite_sso_admin_init' );
}

add_action('admin_menu', 'sonosuite_sso_setup_menu');

?>