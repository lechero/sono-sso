$
wp-cli config create --dbname=MYSQL_DATABASE \
    --dbuser=MYSQL_USER 
    --dbpass=MYSQL_PASSWORD 
    --locale=en_DB

wp-cli core install --url=DOMAIN --title=WP_TITLE --admin_user=ADMIN_USERNAME --admin_password=ADMIN_PASSWORD --admin_email=ADMIN_EMAIL
Success: WordPress installed successfully.


Success: Generated 'wp-config.php' file.


---------

http://localhost:3000/wp-login.php?return_to=https://distribution.veta-music.com/

---------

https://www.veta-music.com/login/?redirect_to=https://www.veta-music.com/wp-admin/

---
Login

<!-- wp:shortcode -->
[play_login_form]
<!-- /wp:shortcode -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

---